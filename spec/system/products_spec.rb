RSpec.describe "Potepan::Products", type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create :taxon }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:not_related_product) { create :product, taxons: [other_taxon] }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "商品詳細ページテスト" do
    expect(page).to have_link "HOME", href: potepan_index_path
    expect(find(".navbar-brand")[:href]).to eq potepan_index_path
    expect(page).to have_selector "li", text: "HOME"
    expect(page).to have_selector "li", text: "#{product.name}"
    expect(page).to have_selector "h2", text: "#{product.name}"
    expect(page).to have_selector "h3", text: "#{product.price}"
    expect(page).to have_selector "p", text: "#{product.description}"
    expect(page.title).to eq "#{product.name} - BIGBAG Store"
  end

  scenario "一覧ページへ戻るリンクテスト" do
    expect(page).to have_link "一覧ページへ戻る"
    click_on "一覧ページへ戻る"
    expect(page).to have_current_path potepan_category_path(taxon.id)
  end

  scenario "関連商品テスト" do
    within ".productsContent" do
      expect(page).to have_content related_products[0].name
      expect(page).to have_content related_products[1].name
      expect(page).to have_content related_products[2].name
      expect(page).to have_content related_products[3].name
      expect(page).not_to have_content related_products[4].name
      expect(page).not_to have_content not_related_product.name
    end
  end

  scenario "関連商品リンク関するテスト" do
    click_on related_products[0].name
    expect(page).to have_current_path potepan_product_path(related_products[0].id)
  end
end
