RSpec.describe "Potepan::Categories", type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "カテゴリーページテスト" do
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
    end
    within ".productsContent" do
      expect(page).to have_content product.name
      expect(page).not_to have_content other_product.name
    end
  end

  scenario "HOMEリンクテスト" do
    expect(page).to have_selector "li", text: "HOME"
    click_link "HOME", match: :prefer_exact
    expect(page).to have_current_path potepan_index_path
  end

  scenario "商品詳細ページリンクテスト" do
    click_on product.name, match: :prefer_exact
    expect(page).to have_current_path potepan_product_path(product.id)
  end
end
