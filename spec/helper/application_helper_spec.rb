RSpec.describe "application_helper", type: :helper do
  describe "ページタイトルのテスト" do
    include ApplicationHelper

    context "引数がある場合" do
      it "引数 - BIGBAG Store と表示すること" do
        expect(full_title("Test Rails Mag")).to eq("Test Rails Mag - BIGBAG Store")
      end
    end

    context "引数がない場合" do
      it "BIGBAG Store と表示すること" do
        expect(full_title("")).to eq("BIGBAG Store")
      end
    end

    context "nilの場合" do
      it "BIGBAG Store と表示すること" do
        expect(full_title(nil)).to eq("BIGBAG Store")
      end
    end
  end
end
