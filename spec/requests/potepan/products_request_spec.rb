RSpec.describe "Products", type: :request do
  describe "productsコントローラshowアクション" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "商品詳細ページにアクセスできること" do
      expect(response).to have_http_status(200)
    end

    it "商品名が含まれていること" do
      expect(response.body).to include product.name
    end

    it "商品紹介が表示されていることt" do
      expect(response.body).to include product.description
    end

    it "商品の価格が表示されていること" do
      expect(response.body).to include "#{product.display_price}"
    end

    it "対象商品が4つ存在すること" do
      expect(controller.instance_variable_get(:@related_products).count).to eq 4
    end
  end
end
