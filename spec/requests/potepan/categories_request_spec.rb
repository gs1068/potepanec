RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  it "カテゴリーページにアクセスできていること" do
    expect(response).to have_http_status(200)
  end

  it "インスタンス変数が定義されていること" do
    expect(controller.instance_variable_get(:@taxonomies).first).to eq taxonomy
    expect(controller.instance_variable_get(:@taxon)).to eq taxon
    expect(controller.instance_variable_get(:@products)).to eq taxon.all_products
  end
end
