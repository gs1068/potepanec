RSpec.describe Spree::Product, type: :model do
  describe "関連商品テスト" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:other_taxon) { create :taxon }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:not_related_product) { create :product, taxons: [other_taxon] }

    it "関連商品をもつ" do
      expect(product.related_products).to include related_product
    end

    it "関連商品を持たない" do
      expect(product.related_products).not_to include not_related_product
    end

    it "関連商品に閲覧中の商品が含まれないこと" do
      expect(product.related_products).not_to include product
    end

    it "関連商品に重複が無いこと" do
      expect(product.related_products.ids.count(related_product.id)).to eq(1)
    end
  end
end
